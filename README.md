Collider -- a software experimenting toolbox

Purpose
=======

Collider is a framework that makes software experimenting less stressful, easier to define and generally simpler to perform.

Rationale
=========

Suppose you are performing experiments in silico (say, performance tests but not only) with a range of variable parameters and a specific workflow that includes generating test cases, running an analysis binary and tracking the time spent on analysis depending on the instance. Usually, this kind of problem is solved by writing a script that loops over the parameter ranges and does the aforementioned task. This solution has several more or less obvious downsides:

- Code duplication by copying and pasting, even in for-loops
- Boilerplate code that tracks progress and saves intermediate results
- Restricted restart and progress tracking capabilities
- Limited many-core usage

After having written a bunch of such scripts (and being over-the-top frustrated with Matlab and shell scripts, copying/pasting of code snippets, rerunnning hour-long experiments/simulations), I have decided to construct a fully general solution.

Usage
=====

collider can be used as an executable or as a library. The executable requires a _config file_ which defines the stages of the experiment and the parameter space. Each stage defines an executable that is called and the call format in Python _format_ convention. The results of each stage are saved into files and, additionally, saved in a machine-readable form.

As a library, collider allows for more flexibility with respect to the definition of experiment stages. That is, one can define custom Python code which will be run as a stage of the experiment. The rationale behind this is to provide maximal flexibility with the library, and coverage of the most common use caes with the executable.

The different parameters of the executable can be seen by executing

`$ collider --help`

The documentation of the library is work in progress.

Configuration file format
=========================

The configuration file is a Python file which should define the following
variables.

* `experiment_name` : The name of the experiment, defines file names for the
  intermediate result folder and the result tables.
  Example: `experiment_name = "myExperiment"`
* `values`: A Python `dict` which describes the parameters that are varied
  during the course of the experiment. The keys correspond to the parameter
  names, the values correspond to the parameter ranges. Any iterable-like data
  type is sufficient, works best with lists and NumPy arrays.
  Example: `values = {'k': [1, 2], 'n': [1, 2, 3, 4]}`
* `stages`: A list that describes individual stages of the experiment. A stage
  is described by a Python `dict` with the following keys:
  * `name`: The name of the stage and the name of the file where the output is
    stored.
  * `exe`: The path to the executable file that should be called
  * `args`: The list of the arguments to the executable file. In this list,
    placeholders for used parameters or output of previous stages (paramater or
    stage name in curly brackets) can be used.
  * `timeout` (optional): The maximal time (in seconds) the stage can be run. If
    this time is exceeded, the executable is terminated.
  * `save_output` (optional): If set to `False`, the output of the stage is saved
    in a file, but not in the internal data structure and will not be available
    for post-processing.
  Example: 
  ```python
  { 
    name: 'generate', 
    exe: '~/projects/mdp-generator/generate.py',
    args: ['--queue', '--states={n}', '--k={k}'],
    timeout: 6000,
    save_output: False
  }
  ```
* `postprocess` (optional): A function with the signature `def postprocess(data:
  pandas.DataFrame, values: Mapping[str, Any])` that takes the results of the
  experiments and applies user-specific post-processing.

Usage as a library
==================

In order to use `collider` as a library, it is needed to define the experiment
parameters, stages, and re-run semantic by hand, and then call the
`run_experiments` function.

Defining the parameters
-----------------------

The experiment parameters are defined by a Python `dict` as described above.

Defining the experiment stages
------------------------------

Each experiment stage is an object of a sub-class of `Stage`. A set of
ready-made sub-classes is available in the library, but one is free to define
their own `Stage`. In order to sub-class `Stage`, one has to define a _name_ of
the stage (e.g., by setting `self.name = …`) and to define the function
`execute(self, value: Mapping[int, Any], stats: Dict[str, Any]) -> Dict[str,
Any]`. The `value` argument is at runtime a tuple with the current parameter
values (sorted by name). The `stats` argument is a `dict` which stores results
from previous stages, ordered by stage. So, if a previous stage is called
`generate`, then `stats['generate']` will store the output of `execute()` in the
`generate` stage for this parameter set.

The return value of `execute` should contain meaningful data that should be used
for post-processing. The output data format is a `dict`, which is then stored in
the _result log_.

Defining the re-run semantics
-----------------------------

In some cases, only some stages of the experiment have to be re-evaluated. In
order to map this requirement, a _predicate_ can be defined. Technically, a
predicate is an object that sub-classes `RerunPredicate`, an interface with a
function `rerun(self, stage, value)`. The return value of `rerun` is `True` if
the `Stage` subclass has to be called again, and `False` if a previously
computed and stored result has to be reused. The library provides several
ready-made predicate classes.
* `AlwaysRerun`: Always run the stage code.
* `RerunIfNeeded`: Run the stage code, unless a computed result already exists.
* `RerunFromStage`: Run the stage code from a given stage only, unless the stage
  has not been run with this set of arguments.
* `RerunOnly`: Run only one stage, unless a computed result for the value exists
* `RerunOnlyForce`: Run only one stage unconditionally.

Calling the library function
----------------------------

To bind all the definitions above together into a running experiment, one has to
call the function
```run_experiments(values, stages, result_log, predicate, groups)``` 
from the `collider` package. This function accepts the following arguments.
* `values`: A Python `dict` describing the parameters, as above.
* `stages`: A list of objects which implement the `Stage` interface.
* `result_log`: A `ResultLog` object which stores the results of an experiment
  in a Pandas `DataFrame`.
* `predicate`: A predicate as defined above.
* `groups`: A list of lists of stage names that can be executed independently.
  Currently, this argument is ignored, in future releases, it will be honored in
  order to gain more from parallel execution.
Furthermore, `run_experiments` accepts the following keyword arguments.
* `job_load`: A number that corresponds to the amount of CPUs a stage for a single
  parameter set is expected to use. This way, the number of worker threads is
  limited to the actual processing power of the hardware.
