# -*- author: Dimitri Scheftelowitsch -*-
# -*- coding:utf-8 -*-

from typing import Any, Dict
from collider import *
import numpy as np
import logging


class InfoStage(Stage):
    def execute(self, value: Any, stats: Dict[str, Any]) -> Dict[str, Any]:
        return {'value': sum(value), 'version': 1.0}


class SecondStage(Stage):
    def execute(self, value: Any, stats: Dict[str, Any]) -> Dict[str, Any]:
        return {'prod': np.product(value), 'version': 1.0}


if __name__ == '__main__':
    values = {
        'x': np.arange(start=10, stop=20, step=5),
        'y': np.arange(start=0, stop=1, step=0.1)
    }
    printer = logging.getLogger("collider")
    printer.setLevel(logging.DEBUG)
    LOG_FORMAT = "%(asctime)s [%(module)s:%(funcName)s] %(levelname)s: %(message)s"
    logging.basicConfig(format=LOG_FORMAT)
    result_log = ResultLog(experiment_name="mock", values=values)
    exec = ExecutableStage(experiment_name="mock", exe="/bin/echo", args=list(values.keys()), patterns=["{x}+{y}"])
    stages = [InfoStage("info"), SecondStage("prod"), exec]
    run_experiments(values, stages, result_log, RerunIfNeeded(result_log), [["info"], ["prod"], ["/bin/echo"]])
