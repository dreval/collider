# -*- author: Dimitri Scheftelowitsch -*-
# -*- coding:utf-8 -*-

"""
A sample config file for collider

Generates a sequence of test files and runs some not very sophisticated analysis on them. Not that much complicated.
"""

import numpy as np
from typing import Mapping, Any
import pandas as pd

experiment_name = 'sample'

values = {
    'x': np.arange(0, 6),
    'y': np.arange(1, 100, 5)
}

stages = [
    {
        'name': 'generate',
        'exe':  'head',
        'args': ['-n 1', '/dev/urandom'],
        'save_output': False
    },
    {
        'name': 'count',
        'exe':  'wc',
        'args': ['-c', '{generate}']
    },
    {
        'name': 'sleep',
        'exe': 'python3',
        'args': ['../../test/cputime.py']
    }
]


def postprocess(data: pd.DataFrame, values: Mapping[str, Any]):
    data.loc[:, 'value'] = pd.Series(0.0, index=data.index)
    data.loc[:, 'relative_deviation'] = pd.Series(0.0, index=data.index)
    print(len(data))